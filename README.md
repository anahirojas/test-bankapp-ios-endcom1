
# Bankapp 
###### Test de apliación móvil


## Acerca de la aplicación

Aplicación de banco que muestra datos traidos desde un JSON, permite ver movimientos, 
tarjetas asociadas, ultima fecha de acceso y asociar una nueva tarjeta.

## Pantallas

* Pantalla principal: Muestra los botones para poder acceder a el resto de las pantallas.
* Cuenta: Muestra el nombre del usuario y su ultima fecha de acceso.
* Movimientos: Permite ver los movimientos realizados en la tarjeta, si es abono el monto se pondra de color verde, si es cargo se pondra en rojo
* Tarjetas: Muestra las tarjetas asociadas, si la tarjeta se encuntra activa el icono de la tarjeta sera negro, si se encuentra desactivada el icono se ostrara gris.
* Asociar tarjeta: Permite ingresar datos y muestra un elemento tipo alert que contine la información de todos
los campos.


## Información técnica de la aplicación
* Lenguaje de programación: Swift
* Software utilizado para el desarrollo : Xcode
* Versión Xcode 12.5 (12E262)


[Video demostración](https://drive.google.com/file/d/15x2EilwoztiHvjNW87TJE4L7b22PjCoA/view?usp=sharing).

