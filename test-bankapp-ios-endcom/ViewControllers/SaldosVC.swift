//
//  SaldosVC.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 29/09/21.
//

import UIKit

class SaldosVC: UIViewController {

    let tableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        // Do any additional setup after loading the view.
    }
    
    func configureTableView() {
        view.addSubview(tableView)
        
        tableView.frame = view.bounds
        tableView.rowHeight = 110
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let textFieldCell = UINib(nibName: "SaldosTVC",bundle: nil)
        tableView.register(textFieldCell, forCellReuseIdentifier: "SaldosTVC")
    }
    
}


extension SaldosVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SaldosTVC") as? SaldosTVC {
                return cell
            }
            
            return UITableViewCell()
    }

}
