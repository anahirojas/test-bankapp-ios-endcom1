//
//  MainMenuVC.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 30/09/21.
//

import UIKit

class MainMenuVC: UIViewController {

    let stackView = UIStackView()
    var botones: [UIButton] = []
    
    let logoImageView = UIImageView()
    let bienvenidoLabel = BATitleLabel(textAligment: .center, fontSize: 24, title: "Bienvenido")
    
    let cuentaButton = BAButton(backgroundColor: .darkBlue, title: "Cuenta")
    let movimientosButton = BAButton(backgroundColor: .darkBlue, title: "Movimientos")
    let saldosButton = BAButton(backgroundColor: .darkBlue, title: "Saldos")
    let tarjetasButton = BAButton(backgroundColor: .darkBlue, title: "Tarjetas")
    let agregarTarjetButton = BAButton(backgroundColor: .darkBlue, title: "Asociar Tarjeta")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .secondarySystemBackground
        configureNavBar()
        configureStackView()
        layoutUI()
        configureLogoImageView()
        configureButtonActions()
    }
    
    func configureNavBar(){
        let image = UIImage(named: "circle.png")
        navigationItem.titleView = UIImageView(image: image)
    }

    private func configureStackView() {
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
    }
    
    
    private func layoutUI() {
        let padding: CGFloat = 60
        let paddingEntreBotones: CGFloat = 25
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(stackView)

        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        botones = [cuentaButton , movimientosButton, saldosButton, tarjetasButton, agregarTarjetButton]
        
        for boton in botones {
            view.addSubview(boton)
            boton.translatesAutoresizingMaskIntoConstraints = false
            
            NSLayoutConstraint.activate([
                boton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: padding),
                boton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -padding),
                boton.heightAnchor.constraint(equalToConstant: 50),
                boton.widthAnchor.constraint(equalToConstant: 120)
            ])
        }

        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: view.topAnchor, constant: padding),
            stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: padding),
            stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -padding),
            stackView.heightAnchor.constraint(equalToConstant: 50),

            cuentaButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 230),
            movimientosButton.topAnchor.constraint(equalTo: cuentaButton.bottomAnchor, constant: paddingEntreBotones),
            tarjetasButton.topAnchor.constraint(equalTo: movimientosButton.bottomAnchor, constant: paddingEntreBotones),
            agregarTarjetButton.topAnchor.constraint(equalTo: tarjetasButton.bottomAnchor, constant: paddingEntreBotones)
        
        ])
    }
    
    
    func configureButtonActions(){
        cuentaButton.addTarget(self, action: #selector(goToCuenta), for: .touchUpInside)
        movimientosButton.addTarget(self, action: #selector(goToMovimientos), for: .touchUpInside)
        tarjetasButton.addTarget(self, action: #selector(goToTarjetas), for: .touchUpInside)
        agregarTarjetButton.addTarget(self, action: #selector(goToAgregarTarjetas), for: .touchUpInside)
    }
    
    @objc func goToCuenta(){
        navigationController?.pushViewController(CuentaVC(), animated: true)
    }
    
    @objc func goToMovimientos(){
        navigationController?.pushViewController(MovimientosVC(), animated: true)
    }
    
    @objc func goToSaldos(){
        navigationController?.pushViewController(SaldosVC(), animated: true)
    }
    
    @objc func goToTarjetas(){
        navigationController?.pushViewController(TarjetasVC(), animated: true)
    }
    
    @objc func goToAgregarTarjetas(){
        navigationController?.pushViewController(FormularioVC(), animated: true)
    }
    
    
    func configureLogoImageView() {
        logoImageView.translatesAutoresizingMaskIntoConstraints = false
        logoImageView.image = #imageLiteral(resourceName: "logoCard")
        
        view.addSubviews(logoImageView, bienvenidoLabel)
        
        NSLayoutConstraint.activate([
            logoImageView.topAnchor.constraint(equalTo: view.topAnchor,constant: 120),
            logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            logoImageView.heightAnchor.constraint(equalToConstant: 50),
            logoImageView.widthAnchor.constraint(equalToConstant: 50),
            
            bienvenidoLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 5),
            bienvenidoLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        ])
    }
}
