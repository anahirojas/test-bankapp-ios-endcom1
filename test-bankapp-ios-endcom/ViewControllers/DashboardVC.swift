//
//  DashboardVC.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 29/09/21.
//

import UIKit

class DashboardVC: UIViewController {

    let headerView = UIView()
    let seccionCuenta = UIView()
    var itemViews: [UIView] = []
    let contentView = UIView()
    


    override func viewDidLoad() {
        super.viewDidLoad()
        //title = "BankApp"
        
        configureNavBar()
        configViewController()
        configureUIElements()
        layoutUI()
        
    }
    
    func configureNavBar(){
        let image = UIImage(named: "circle.png")
        navigationItem.titleView = UIImageView(image: image)
    }
    
    
    func configViewController() {
        view.backgroundColor = .systemRed
    }
    
    func add(childVC: UIViewController, to containnerView: UIView) {
        addChild(childVC)
        containnerView.addSubview(childVC.view)
        childVC.view.frame = containnerView.bounds
        childVC.didMove(toParent: self)
    }
    
    
    func configureUIElements() {
        //self.add(childVC: CuentaVC(), to: self.seccionCuenta)
        self.add(childVC: CuentaVC (), to: self.headerView)
        
    }
    
    
    func layoutUI() {
        let padding: CGFloat = 20
        view.addSubview(headerView)
        headerView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            

        //headerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 350),
        headerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 350),
        headerView.heightAnchor.constraint(equalToConstant: 140),
        headerView.widthAnchor.constraint(equalToConstant: 100),
//        headerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: padding),
//        headerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -padding),
        ])
    }
    
    


}
