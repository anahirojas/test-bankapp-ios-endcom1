//
//  FormularioVC.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 30/09/21.
//

import UIKit

class FormularioVC: UIViewController {

    let stackView = UIStackView()
    var itemViews: [UIView] = []
    
    let logoImageView = UIImageView()
    let asociarTarjetaLabel = BALabel(textAligment: .center, text: "Asociar Tarjeta")
    let numeroTarjetaTF = BATextField(placeholder: "Numero de Tarjeta")
    let cuentaTF = BATextField(placeholder: "Cuenta")
    let issureTF = BATextField(placeholder: "Issure")
    let nombreTarjetaTF = BATextField(placeholder: "Nombre de Tarjeta")
    let marcaTF = BATextField(placeholder: "Marca")
    let estatusTF = BATextField(placeholder: "Estatus")
    let saldoTF = BATextField(placeholder: "Saldo")
    let tipoCuentaTF = BATextField(placeholder: "Tipo de cuenta")
    
    let botonCancelar = BAButton(backgroundColor: .systemGray2, title: "Cancelar")
    let botonAceptar = BAButton(backgroundColor: .systemGreen, title: "Aceptar")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .secondarySystemBackground
        configureNavBar()
        configureLogoImageView()
        configureStackView()
        configureButtonActions()
        layoutUI()
    }
    
    private func configureStackView() {
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        
    }

    private func layoutUI() {
        let padding: CGFloat = 20
        let textFieldPadding: CGFloat = 14
        
        view.addSubviews(stackView, botonCancelar, botonAceptar)

        stackView.translatesAutoresizingMaskIntoConstraints = false
        itemViews = [numeroTarjetaTF, cuentaTF, issureTF, nombreTarjetaTF, marcaTF, estatusTF, saldoTF, tipoCuentaTF]
        
        for itemView in itemViews {
            view.addSubview(itemView)
            itemView.translatesAutoresizingMaskIntoConstraints = false
            
            NSLayoutConstraint.activate([
                itemView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: padding),
                itemView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -padding),
                itemView.heightAnchor.constraint(equalToConstant: 34)
            ])
        }

        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: view.topAnchor, constant: padding),
            stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: padding),
            stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -padding),
            stackView.heightAnchor.constraint(equalToConstant: 50),

            numeroTarjetaTF.topAnchor.constraint(equalTo: view.topAnchor, constant: 230),
            
            cuentaTF.topAnchor.constraint(equalTo: numeroTarjetaTF.bottomAnchor, constant: textFieldPadding),
            issureTF.topAnchor.constraint(equalTo: cuentaTF.bottomAnchor, constant: textFieldPadding),
            nombreTarjetaTF.topAnchor.constraint(equalTo: issureTF.bottomAnchor, constant: textFieldPadding),
            marcaTF.topAnchor.constraint(equalTo: nombreTarjetaTF.bottomAnchor, constant: textFieldPadding),
            estatusTF.topAnchor.constraint(equalTo: marcaTF.bottomAnchor, constant: textFieldPadding),
            saldoTF.topAnchor.constraint(equalTo: estatusTF.bottomAnchor, constant: textFieldPadding),
            tipoCuentaTF.topAnchor.constraint(equalTo: saldoTF.bottomAnchor, constant: textFieldPadding),
            
            botonCancelar.topAnchor.constraint(equalTo: tipoCuentaTF.bottomAnchor, constant: 50),
            botonCancelar.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 60),
            botonCancelar.widthAnchor.constraint(equalToConstant: 120),
            
            botonAceptar.topAnchor.constraint(equalTo: tipoCuentaTF.bottomAnchor, constant: 50),
            botonAceptar.leadingAnchor.constraint(equalTo: botonCancelar.trailingAnchor, constant: 55),
            botonAceptar.widthAnchor.constraint(equalToConstant: 120)

        ])
    }
    
    func configureButtonActions(){
        botonCancelar.addTarget(self, action: #selector(goToMainMenu), for: .touchUpInside)
        botonAceptar.addTarget(self, action: #selector(sendAlert), for: .touchUpInside)
    }
    
    @objc func goToDashboard(){
        self.navigationController?.pushViewController(MainMenuVC(), animated: true)
    }
    
    @objc func sendAlert(){
        guard let numeroTarjetaText = numeroTarjetaTF.text, !numeroTarjetaText.isEmpty else {
            return
        }
        guard let cuentaText = cuentaTF.text, !numeroTarjetaText.isEmpty else {
            return
        }
        guard let issureText = issureTF.text, !numeroTarjetaText.isEmpty else {
            return
        }
        guard let nombreTarjetaText = nombreTarjetaTF.text, !numeroTarjetaText.isEmpty else {
            return
        }
        guard let marcaText = marcaTF.text, !numeroTarjetaText.isEmpty else {
            return
        }
        guard let estatusText = estatusTF.text, !numeroTarjetaText.isEmpty else {
            return
        }
        guard let saldoText = saldoTF.text, !numeroTarjetaText.isEmpty else {
            return
        }
        guard let tipoCuenta = tipoCuentaTF.text, !numeroTarjetaText.isEmpty else {
            return
        }
        
        let mensaje = "Número Tarjeta: \(numeroTarjetaText) \n Cuenta: \(cuentaText) \n Issure: \(issureText) \n Nombre: \(nombreTarjetaText) \n Marca: \(marcaText) \n  Estatus: \(estatusText) \n Saldo: \(saldoText) \n Tipo Cuenta: \(tipoCuenta) \n"
        
        let alert = UIAlertController(title: "Tarjeta Guardada", message: mensaje, preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
    }
    
    func configureLogoImageView() {
        logoImageView.translatesAutoresizingMaskIntoConstraints = false
        logoImageView.image = #imageLiteral(resourceName: "darkCard")
        
        view.addSubviews(logoImageView, asociarTarjetaLabel)
        
        NSLayoutConstraint.activate([
            logoImageView.topAnchor.constraint(equalTo: view.topAnchor,constant: 120),
            logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            logoImageView.heightAnchor.constraint(equalToConstant: 40),
            logoImageView.widthAnchor.constraint(equalToConstant: 40),
            
            asociarTarjetaLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: 5),
            asociarTarjetaLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        ])
    }
    
    //MARK:- NavigationBar
    
    func configureNavBar(){
        let image = UIImage(named: "circle.png")
        navigationItem.titleView = UIImageView(image: image)
        
    }
    
    @objc func goToMainMenu() {
        let mainMenu = MainMenuVC()
        self.navigationController?.pushViewController(mainMenu, animated: true)
    }
    
}
