//
//  CuentaVC.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 30/09/21.
//

import UIKit

class CuentaVC: UIViewController {
    let tableView = UITableView()
    var cuenta: [Cuenta] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .blue
        tableView.frame = CGRect(x: tableView.frame.origin.x, y: tableView.frame.origin.y, width: tableView.frame.size.width, height: tableView.contentSize.height)
        configureTableView()
        configureNavBar()
        getRequest()
    }
    
    
    func configureNavBar(){
        let image = UIImage(named: "circle.png")
        navigationItem.titleView = UIImageView(image: image)
    }
    
    
    func configureTableView() {
        view.addSubview(tableView)
        
        tableView.frame = view.bounds
        tableView.rowHeight = 80
        
        tableView.isScrollEnabled = true
        tableView.delegate = self
        tableView.dataSource = self
        tableView.removeExcessCells()
        
        let textFieldCell = UINib(nibName: "CuentaTVC",bundle: nil)
        tableView.register(textFieldCell, forCellReuseIdentifier: "CuentaTVC")
    }

    
    
    func getRequest() {
        NetworkManager.shared.getCuentaInfo { [weak self] result in
            guard let self = self else { return}
            
            switch result {
            case .success(let response):
                self.updateUI(with: response.cuenta)
                
                print("Cuenta:)\(response.cuenta.count)")
                
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    
    func updateUI(with cuenta: [Cuenta]){
        if cuenta.isEmpty {
            print("No hay movimientos")
        } else {
            self.cuenta = cuenta
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.view.bringSubviewToFront(self.tableView)
            }
        }
    }
    

    
}



extension CuentaVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return cuenta.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CuentaTVC") as? CuentaTVC {

            cell.set(cuenta: cuenta[indexPath.row])
            cell.backgroundColor = .clear
            cell.backgroundView = UIView()
            cell.selectedBackgroundView = UIView()
            return cell
            }
        
            return UITableViewCell()

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
    
}
