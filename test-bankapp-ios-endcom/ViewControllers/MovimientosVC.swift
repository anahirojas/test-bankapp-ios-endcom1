//
//  MovimientosVC.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 29/09/21.
//

import UIKit

class MovimientosVC: UIViewController {

    let tableView = UITableView()
    var movimiento: [Movimiento] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        getRequest()
        configureNavBar()
    }
    
    
    func configureNavBar(){
        let image = UIImage(named: "circle.png")
        navigationItem.titleView = UIImageView(image: image)
    }
    
    
    func configureTableView() {
        view.addSubview(tableView)
        
        tableView.frame = view.bounds
        tableView.rowHeight = 80
        
        tableView.isScrollEnabled = true
        tableView.delegate = self
        tableView.dataSource = self
        
        
        
        let textFieldCell = UINib(nibName: "MovimientosTVC",bundle: nil)
        tableView.register(textFieldCell, forCellReuseIdentifier: "MovimientosTVC")
    }
    
    
    func getRequest() {
        NetworkManager.shared.getMovimientosInfo { [weak self] result in
            guard let self = self else { return}
            
            switch result {
            case .success(let response):
                self.updateUI(with: response.movimientos)
                
                print("MOVIMIENTO:)\(response.movimientos.count)")
                
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    
    func updateUI(with movimientos: [Movimiento]){
        if movimientos.isEmpty {
            print("No hay movimientos")
        } else {
            self.movimiento = movimientos
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.view.bringSubviewToFront(self.tableView)
            }
        }
    }
    
}

extension MovimientosVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movimiento.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MovimientosTVC") as? MovimientosTVC {

            cell.set(movimiento: movimiento[indexPath.row])
            return cell
            }
        
            return UITableViewCell()
        
        
    }
    
    
    
}
