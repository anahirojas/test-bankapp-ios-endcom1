//
//  BotonUnTituloVC.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 30/09/21.
//

import UIKit

class BotonUnTituloVC: UIViewController {

    let tableView = UITableView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        
    }
    
    func configureTableView() {
        view.addSubview(tableView)
        
        tableView.frame = view.bounds
        tableView.rowHeight = 80
        
        tableView.isScrollEnabled = true
        tableView.delegate = self
        tableView.dataSource = self
        
        
        
        let textFieldCell = UINib(nibName: "BotonTitulo1TVC",bundle: nil)
        tableView.register(textFieldCell, forCellReuseIdentifier: "BotonTitulo1TVC")
    }

    
}

extension BotonUnTituloVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "BotonTitulo1TVC") as? BotonTitulo1TVC {

            }
        
            return UITableViewCell()
        
        
    }
    
    
    
}

