//
//  TarjetasVC.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 29/09/21.
//

import UIKit

class TarjetasVC: UIViewController {

    let tableView = UITableView()
    var tarjeta: [Tarjeta] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        getRequest()
        configureNavBar()
    }
    
    
    func configureNavBar(){
        let image = UIImage(named: "circle.png")
        navigationItem.titleView = UIImageView(image: image)
    }
    
    
    func configureTableView() {
        view.addSubview(tableView)
        
        tableView.frame = view.bounds
        tableView.rowHeight = 105
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let textFieldCell = UINib(nibName: "TarjetasTVC",bundle: nil)
        tableView.register(textFieldCell, forCellReuseIdentifier: "TarjetasTVC")
    }
    
    
    func getRequest() {
        NetworkManager.shared.getTarjetasInfo { [weak self] result in
            guard let self = self else { return}
            
            switch result {
            case .success(let response):
                self.updateUI(with: response.tarjetas)
                
                print("TARJETA:)\(response.tarjetas.count)")
                
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    
    func updateUI(with tarjetas: [Tarjeta]){
        if tarjetas.isEmpty {
            print("No hay tarjetas")
        } else {
            self.tarjeta = tarjetas
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.view.bringSubviewToFront(self.tableView)
            }
        }
    }
    
}


extension TarjetasVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tarjeta.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "TarjetasTVC") as? TarjetasTVC {
            cell.set(tarjeta: tarjeta[indexPath.row])
                return cell
            }
            
            return UITableViewCell()
    }

}
