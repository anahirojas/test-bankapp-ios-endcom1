//
//  String+Ext.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 30/09/21.
//

import Foundation

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
