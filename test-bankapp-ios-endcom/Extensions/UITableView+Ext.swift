//
//  UITableView+Ext.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 30/09/21.
//

import UIKit

extension UITableView{
    func removeExcessCells() {
        tableFooterView = UIView(frame: .zero)
    }
}

