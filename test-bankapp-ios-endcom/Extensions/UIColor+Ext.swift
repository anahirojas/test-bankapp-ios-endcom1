//
//  UIColor+Ext.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 30/09/21.
//

import UIKit

extension UIColor {
    static let darkBlue = #colorLiteral(red: 0.1393021345, green: 0.2012238503, blue: 0.2972669005, alpha: 1)
    static let mediumBlue = #colorLiteral(red: 0.2608607709, green: 0.611803472, blue: 0.7811834216, alpha: 1)
    static let green = #colorLiteral(red: 0.2431372549, green: 0.7450980392, blue: 0.5725490196, alpha: 1)
}
