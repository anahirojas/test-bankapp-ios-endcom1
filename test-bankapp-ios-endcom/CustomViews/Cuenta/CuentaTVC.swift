//
//  CuentaTVC.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 30/09/21.
//

import UIKit

class CuentaTVC: UITableViewCell {

    @IBOutlet weak var usuarioLabel: UILabel!
    @IBOutlet weak var ultimaSesionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func set(cuenta: Cuenta) {
        let fecha = cuenta.ultimaSesion
    
        usuarioLabel.text = cuenta.nombre
        ultimaSesionLabel.text = "Último inicio " + fecha.prefix(10)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
