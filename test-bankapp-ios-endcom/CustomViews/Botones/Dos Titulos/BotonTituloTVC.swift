//
//  BotonTituloTVC.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 30/09/21.
//

import UIKit

class BotonTituloTVC: UITableViewCell {

    @IBOutlet weak var boton1: UIButton!
    @IBOutlet weak var boton2: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func configurarBotones() {
        boton1.setTitle("Mis Cuentas", for: .normal)
        boton2.setTitle("Enviar Dinero", for: .disabled)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
