//
//  BotonTitulo1TVC.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 30/09/21.
//

import UIKit

class BotonTitulo1TVC: UITableViewCell {

    @IBOutlet weak var botonUnTitulo: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        botonUnTitulo.setTitle("+ Agregar una tarjeta de débito o crédito", for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
