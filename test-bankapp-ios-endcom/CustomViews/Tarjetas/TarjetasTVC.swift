//
//  TarjetasTVC.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 29/09/21.
//

import UIKit

class TarjetasTVC: UITableViewCell {

    @IBOutlet weak var imageTarjeta: UIImageView!
    @IBOutlet weak var estadoTarjetaLabel: UILabel!
    @IBOutlet weak var numTarjetaLabel: UILabel!
    @IBOutlet weak var titularLabel: UILabel!
    @IBOutlet weak var saldoLabel: UILabel!
    @IBOutlet weak var nombreTarjetaLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func set(tarjeta: Tarjeta) {
        estadoTarjetaLabel.text = tarjeta.estado
        numTarjetaLabel.text = tarjeta.tarjeta
        titularLabel.text = tarjeta.tipo
        saldoLabel.text = "$" + String(tarjeta.saldo)
        nombreTarjetaLabel.text = tarjeta.nombre
        
        if estadoTarjetaLabel.text == "desactivada" {
            imageTarjeta.image = #imageLiteral(resourceName: "lightCard")
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
