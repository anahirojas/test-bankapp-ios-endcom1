//
//  BATitleLabel.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 30/09/21.
//

import UIKit

class BATitleLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //Custom initializer
    convenience init(textAligment: NSTextAlignment, fontSize: CGFloat, title: String) {
        self.init(frame: .zero)
        self.textAlignment = textAlignment
        self.font = UIFont.systemFont(ofSize: fontSize, weight: .bold)
        self.text = title
    }
    
    
    private func configure() {
        //.label change depending on dark mode
        textColor = .green
        adjustsFontSizeToFitWidth = true
        minimumScaleFactor = 0.90
        //If the title is to long how it's going to manage it, it will add ... at the end
        lineBreakMode = .byTruncatingTail
        translatesAutoresizingMaskIntoConstraints = false
    }

}
