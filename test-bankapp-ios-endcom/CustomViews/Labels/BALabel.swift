//
//  BALabel.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 30/09/21.
//

import UIKit

class BALabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //Custom initializer
    convenience init(textAligment: NSTextAlignment, text: String) {
        self.init(frame: .zero)
        self.textAlignment = textAlignment
        self.text = text
    }
    
    
    private func configure() {
        textColor                         = .secondaryLabel
        font                              = UIFont.preferredFont(forTextStyle: .body)
        adjustsFontForContentSizeCategory = true
        adjustsFontSizeToFitWidth         = true
        minimumScaleFactor                = 0.75
        //If the title is to long how it's going to manage it
        lineBreakMode                     = .byWordWrapping
        translatesAutoresizingMaskIntoConstraints = false
        
    }

}
