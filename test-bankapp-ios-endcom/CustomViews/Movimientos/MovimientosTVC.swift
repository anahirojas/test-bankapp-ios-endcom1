//
//  MovimientosTVC.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 29/09/21.
//

import UIKit

class MovimientosTVC: UITableViewCell {

    @IBOutlet weak var conceptoLabel: UILabel!
    
    @IBOutlet weak var costoLabel: UILabel!
    
    @IBOutlet weak var fechaLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    func set(movimiento: Movimiento) {
        costoLabel.text = movimiento.monto
        conceptoLabel.text = movimiento.descripcion
        fechaLabel.text = movimiento.fecha
        
        if movimiento.tipo.rawValue == "abono" {
            costoLabel.textColor = .systemGreen
        } else {
            costoLabel.textColor = .red
        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
