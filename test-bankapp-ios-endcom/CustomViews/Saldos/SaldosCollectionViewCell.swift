//
//  SaldosCollectionViewCell.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 30/09/21.
//

import UIKit

class SaldosCollectionViewCell: UICollectionViewCell {

    @IBOutlet var tituloLabel: UILabel!
    @IBOutlet var saldoLabel: UILabel!
    
    static let identifier = "SaldosCollectionViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "SaldosCollectionViewCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func set(saldo: Saldo) {
        tituloLabel.text = "Titulo"
        saldoLabel.text = String(saldo.saldoGeneral)
    }

}
