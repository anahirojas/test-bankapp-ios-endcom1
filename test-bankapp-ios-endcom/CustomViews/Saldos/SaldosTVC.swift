//
//  SaldosTVC.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 29/09/21.
//

import UIKit

class SaldosTVC: UITableViewCell {

    @IBOutlet var collectionView: UICollectionView!
    
    //var saldos = Saldo()
    
    static let identifier = "SaldosTVC"
    
    static func nib() ->UINib {
        return UINib(nibName: "SaldosTVC", bundle: nil)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
