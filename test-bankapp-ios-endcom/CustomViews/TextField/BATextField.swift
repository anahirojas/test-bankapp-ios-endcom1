//
//  BATextField.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 30/09/21.
//

import UIKit

class BATextField: UITextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(placeholder: String) {
        self.init(frame: .zero)
        self.placeholder = placeholder
    }


    private func configure(){
        translatesAutoresizingMaskIntoConstraints = false
        
        layer.cornerRadius = 5
        layer.borderWidth  = 2
        layer.borderColor  = UIColor.systemGray4.cgColor
        
        //. label change between black and white when using (or not) darkmode
        textColor                 = .label
        tintColor                 = .label
        textAlignment             = .center
        font                      = UIFont.preferredFont(forTextStyle: .title2)
        font                      = UIFont.systemFont(ofSize: 14)
        adjustsFontSizeToFitWidth = true
        
        backgroundColor    = .tertiarySystemBackground
        autocorrectionType = .no
        returnKeyType      = .go
        //Add an "x" at the end of the textfield so you can erase all the text
        clearButtonMode    = .whileEditing
    }
}
