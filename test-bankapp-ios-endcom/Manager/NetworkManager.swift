//
//  NetworkManager.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 29/09/21.
//

import Foundation

class NetworkManager {
    
    static let shared   = NetworkManager()
    private let baseURL = "http://bankapp.endcom.mx/api/bankappTest/"
    
    private init() {}
    

//MARK:- Movimientos

    func getMovimientosInfo(completed: @escaping (Result<MovimientosResponse, BAError>) -> Void) {
        let endpoint = baseURL + "movimientos"
        print("DEBUG:\(endpoint)")
        
        guard let url = URL(string: endpoint) else {
            completed(.failure(.unableToComplete))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in

            if let _ = error {
                completed(.failure(.unableToComplete))
            }

            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                completed(.failure(.invalidResponse))
                return
            }
            
            
            guard let data = data else {
                completed(.failure(.invalidData))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                let movimientos = try decoder.decode(MovimientosResponse.self, from: data)
                completed(.success(movimientos))
            } catch {
                completed(.failure(.invalidData))
            }
        }
        task.resume()
    }
    
//MARK: - Tarjetas
    
    func getTarjetasInfo(completed: @escaping (Result<TarjetaResponse, BAError>) -> Void) {
        let endpoint = baseURL + "tarjetas"
        print("DEBUG:\(endpoint)")
        
        guard let url = URL(string: endpoint) else {
            completed(.failure(.unableToComplete))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in

            if let _ = error {
                completed(.failure(.unableToComplete))
            }

            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                completed(.failure(.invalidResponse))
                return
            }
            
            
            guard let data = data else {
                completed(.failure(.invalidData))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                let tarjetas = try decoder.decode(TarjetaResponse.self, from: data)
                completed(.success(tarjetas))
            } catch {
                completed(.failure(.invalidData))
            }
        }
        task.resume()
    }
    
    
    //MARK: - Cuenta
        
        func getCuentaInfo(completed: @escaping (Result<CuentaResponse, BAError>) -> Void) {
            let endpoint = baseURL + "cuenta"
            print("DEBUG:\(endpoint)")
            
            guard let url = URL(string: endpoint) else {
                completed(.failure(.unableToComplete))
                return
            }
            
            let task = URLSession.shared.dataTask(with: url) { data, response, error in

                if let _ = error {
                    completed(.failure(.unableToComplete))
                }

                guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                    completed(.failure(.invalidResponse))
                    return
                }
                
                
                guard let data = data else {
                    completed(.failure(.invalidData))
                    return
                }
                
                do {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601
                    let cuenta = try decoder.decode(CuentaResponse.self, from: data)
                    completed(.success(cuenta))
                } catch {
                    completed(.failure(.invalidData))
                }
            }
            task.resume()
        }
}

