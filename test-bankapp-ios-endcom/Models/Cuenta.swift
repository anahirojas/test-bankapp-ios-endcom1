//
//  Cuenta.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 30/09/21.
//

import Foundation

// MARK: - Cuenta
struct CuentaResponse: Codable {
    let cuenta: [Cuenta]
}

// MARK: - CuentaElement
struct Cuenta: Codable {
    let cuenta: Int
    let nombre, ultimaSesion: String
}
