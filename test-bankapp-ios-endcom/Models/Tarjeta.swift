//
//  Tarjeta.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 30/09/21.
//

import Foundation

// MARK: - Tarjeta
struct TarjetaResponse: Codable {
    let tarjetas: [Tarjeta]
}

// MARK: - TarjetaElement
struct Tarjeta: Codable {
    let tarjeta, nombre: String
    let saldo: Int
    let estado, tipo: String
}
