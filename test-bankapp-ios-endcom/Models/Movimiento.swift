//
//  Movimiento.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 29/09/21.
//

import Foundation


// MARK: - Response
struct MovimientosResponse: Codable {
    let movimientos: [Movimiento]
}

// MARK: - Movimiento
struct Movimiento: Codable {
    let fecha, descripcion, monto: String
    let tipo: Tipo
    let id: Int
}

enum Tipo: String, Codable {
    case abono = "abono"
    case cargo = "cargo"
}

