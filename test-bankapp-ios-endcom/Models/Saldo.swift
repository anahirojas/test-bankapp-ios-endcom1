//
//  Saldo.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 30/09/21.
//

import Foundation
// MARK: - Saldos
struct Saldos: Codable {
    let saldos: [Saldo]
}

// MARK: - Saldo
struct Saldo: Codable {
    let cuenta, saldoGeneral, ingresos, gastos: Int
    let id: Int
}


