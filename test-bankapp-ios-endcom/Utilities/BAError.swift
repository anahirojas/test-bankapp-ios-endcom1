//
//  BAError.swift
//  test-bankapp-ios-endcom
//
//  Created by Anahi Rojas on 29/09/21.
//

import Foundation

enum BAError: String, Error {
    case unableToComplete   = "Unable to complete your request. Pleasse check your internet connection."
    case invalidResponse    = "Invalid response from the server. Please try again."
    case invalidData        = "The data recived from the server was invalid. Please try again."
}
